# Hi I'm Chris

<div markdown="1">

<img align="right" float="left" margin-left="25" height="200" src="https://gitlab.com/vickeryc/vickeryc/-/raw/main/images/cv-recent-sized.png" title="headshot of Christopher Vickery, who meets expectations">

I am a Senior Tech Writer and Content Manager in Pacifica, California. I have written in everything from Microsoft Word to Madcap Flare to Markdown and Asciidoc. 

I have worked in software and hardware, documenting SaaS PLM systems, APIs and SDKs, even virtual reality experiences and helmets. I have worked as a solo writer owning the entire doc function and I have worked as part of a larger team. I have been 100% remote (playing my records and annoying my pets), and I have been in the office full-time, eyeing the snacks.

Accessiblity and inclusive language are important to me. I write succinctly and communicate well. I listen and find the way forward. 

[LinkedIn profile](https://www.linkedin.com/in/christopher-vickery-954b976/)

</div>

## Samples

<div markdown="1">

<!--[<img align="right" width="325" margin-left="25px" border="1px" alt="image of the documentation site for Strivr's software development kit for Unity" bordercolor="solid black" src="/images/strivr-dev-docs-site.png">](https://developer.strivr.com/docs/sdk/latest/index.html)-->

### [Developer Doc Site](https://developer.strivr.com/docs/sdk/latest/index.html) 

[<img align="center" alt="image of the documentation site for Strivr's software development kit for Unity" title="link to the developer documentation site for Strivr's SDK for Unity" width="650px" src="https://gitlab.com/vickeryc/vickeryc/-/raw/main/images/strivr-dev-docs-site-border.png">](https://developer.strivr.com/docs/sdk/latest/index.html)

- Interesting pages:
    - [Creating an experience](https://developer.strivr.com/docs/sdk/latest/unity/exp-tracking.html)
    - [How to track events](https://developer.strivr.com/docs/sdk/latest/unity/how-to-track-events.html)
    - [Working with data in the Strivr Portal](https://developer.strivr.com/docs/sdk/latest/unity/working-with-data.html)
- I authored and maintained this docs site for a SDK for external devs linking their VR training experiences to Strivr's online portal, which is used to analyze training results.
- The site is generated using [Antora](https://antora.org/).
- The site is authored in [Asciidoc](https://asciidoc.org/).

### [REST API docs site](https://redacted-rest-api-295119.gitlab.io/) 

[<img align="center" alt="image of a REST API docs site for a SaaS PLM company" title="link to the REST API docs site for a SaaS PLM company" width="650px" src="https://gitlab.com/vickeryc/vickeryc/-/raw/main/images/rest-api-site.png">](https://redacted-rest-api-295119.gitlab.io/)

- I am converting this REST API docs site from Madcap Flare using Hugo static site generator.
- The site is hosted on [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/).
- The site is authored in [Markdown](https://www.markdownguide.org/).
- The site conversion is still in progress as of 11/04/2024.

</div>

<div markdown="1">

<!--[<img align="right" width="325" margin-left="25px" border="1" alt="image of the homepage for Strivr's support and help site" src="/images/strivr-support-site.png">](https://support.strivr.com)-->

### [Online help/support site](https://support.strivr.com)

[<img align="center" alt="image of the homepage for Strivr's support and help site" title="link to the support and help site for Strivr"  width="650px" src="https://gitlab.com/vickeryc/vickeryc/-/raw/main/images/strivr-support-site-sized.png">](https://support.strivr.com)

- Interesting pages: 
    - [Release notes](https://support.strivr.com/docs/release_notes_portal.htm) - Release notes for the Strivr customer portal for data analytics and VR experience management
    - [Strivr System Overview](https://support.strivr.com/docs/system-overview.htm) - High-level description of Strivr's services
    - [Training Day](https://support.strivr.com/system-topics/training-day.htm) - Best practices for customer facilitators delivering VR training
    - [Online Portal Tour Video](https://www.youtube.com/watch?v=0cZin_Sou2o) - Overview of the Strivr Portal
- I designed, authored, and maintained this site for Strivr, a VR training company.
- The site is authored in and generated using [Madcap Flare](https://www.madcapsoftware.com/products/flare/).

### [Getting started guide](https://support.strivr.com/downloads/strivr-welcome-immersive-learning-neo3-1.3.pdf)

[<img align="center" alt="image of the cover for a booklet titled Welcome to Immersive Learning" title="a booklet titled Welcome to Immersive Learning"  width="450px" src="https://gitlab.com/vickeryc/vickeryc/-/raw/main/images/welcome-to-immersive-learning.png">](https://support.strivr.com/downloads/strivr-welcome-immersive-learning-neo3-1.3.pdf)

- I created this printed booklet for Strivr, a VR training company.
- The booklet was shipped to customers receiving a Virtual Reality training kit.
- The booklet contains setup instructions, FAQs, and troubleshooting.
- I created this booklet using Adobe InDesign, Adobe Photoshop, and Adobe Illustrator.
- I led the project from start to finish, including the design and layout, and worked with offset printers to deliver the project.

### [New feature intro](https://support.strivr.com/downloads/strivr-acclimation-and-strivr-home.pdf)

[<img align="center" alt="image of the front page of on overview document detailing a revamped experience for VR learners" title="an overview document detailing a revamped experience for VR learners" height="650px" src="https://gitlab.com/vickeryc/vickeryc/-/raw/main/images/strivr-acclimation-and-strivr-home.png">](https://support.strivr.com/downloads/strivr-acclimation-and-strivr-home.pdf)

- I created this overview document for Strivr, a VR training company.
- This  document details a revamped experience for Virtual Reality learners.
- The document includes a redesigned virtual lobby and updated flow for introductory screens.
- I created this document using Google docs, Adobe Photoshop, and Adobe Illustrator.

### [User manual](https://support.strivr.com/downloads/strivr_user-manual-neo3.pdf)

[<img align="center" alt="image of the cover for a virtual reality training user manual" title="a virtual reality training user manual" height="650px" src="https://gitlab.com/vickeryc/vickeryc/-/raw/main/images/strivr_user-manual-neo3.png">](https://support.strivr.com/downloads/strivr_user-manual-neo3.pdf)

- I created this user manual for Strivr, a VR training company.
- This user manual includes all the information needed to use a virtual reality training system.
- I created this document using Adobe InDesign, Adobe Photoshop, and Adobe Illustrator.

</div>